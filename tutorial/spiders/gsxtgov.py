import scrapy
import execjs
import re
import subprocess
from scrapy.crawler import CrawlerProcess

class GsxtgovSpider(scrapy.Spider):
    name = 'gsxtgov'
    allowed_domains = ['www.gsxt.gov.cn']
    start_urls = ['http://www.gsxt.gov.cn/']

    custom_settings = {
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',
        'HTTPERROR_ALLOWED_CODES': [521],
    }

    def parse(self, response):
        self.logger.info('调用parse %s', response.url)
        js_cookies = self.getJsCookie1T(response)
        self.logger.info('js_cookies %s', js_cookies)
        if js_cookies is not None:
            request = response.follow('', callback=self.parse_cookie_2t)
            request.cookies = js_cookies
            request.cb_kwargs['js_cookies'] = js_cookies
            yield request

    def parse_cookie_2t(self, response, js_cookies):
        self.logger.info('调用parse_cookie_2t %s', response.url)
        js2_cookies = self.getJsCookie2T(response, js_cookies)
        self.logger.info('js_cookies %s', js2_cookies)
        if js2_cookies is not None:
            request = response.follow('', dont_filter = True, callback=self.parse_redirect)
            request.cookies = js2_cookies
            request.cb_kwargs['js_cookies'] = js2_cookies
            yield request
    
    def parse_redirect(self, response, js_cookies):
        self.logger.info('调用parse_redirect %s', response.url)
        search_box = response.css('form.search_index_box')
        action = search_box.attrib['action']
        method = search_box.attrib['method']
        tab = 'ent_tab'
        geetest_challenge = ''
        geetest_validate = ''
        geetest_seccode = ''
        token = '2016'
        searchword = ''
        pass

    def getJsCookie1T(self, response):
        jsluid_h = response.headers['Set-Cookie'].decode('utf-8').split('=')[1].split(';')[0]
        js_src = response.css('script::text').get()
        js_func_body = js_src.replace('document.cookie', 'let cookie').replace('location.href=location.pathname+location.search', 'return cookie;')
        js_func = 'function getCookie() {' + js_func_body + '}'
        jscontext = execjs.compile(js_func)
        cookie_str = jscontext.call("getCookie")
        jsl_clearance = cookie_str.split('=')[1].split(';')[0]
        # js_cookie = f"__jsluid_h={jsluid_h}; __jsl_clearance={jsl_clearance}"
        return {
            '__jsluid_h': jsluid_h,
            '__jsl_clearance': jsl_clearance
        }

    def getJsCookie2T(self, response, js_cookies):
        data = response.text
        js = re.findall('<script>(.*?)</script>', data)[0]
        js_added_head = "const document = { cookie: {} };const location = {};const navigator = {userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'};const window = { document, location, navigator };const alert = function(str) {};"
        js = js_added_head + js
        js = re.sub(r'location\[.+\]=[^;]+;', "console.log(document['cookie']);", js)
        with open(r'js-reverse/temp1.js', 'w', encoding='utf-8') as f:
            f.write(js)
        ret=subprocess.getstatusoutput("node js-reverse/temp1.js")
        jsl_clearance=ret[1].split(";")[0].split("=")[1]
        # jsluid_h = js_cookies['__jsluid_h']
        # js_cookie = f"{js_cookie.split(';')[0]}; __jsl_clearance={jsl_clearance}"
        js_cookies['__jsl_clearance'] = jsl_clearance
        return js_cookies


def main():
    process = CrawlerProcess(settings={
    })
    process.crawl(GsxtgovSpider)
    process.start()

if __name__ == "__main__":
    main()

