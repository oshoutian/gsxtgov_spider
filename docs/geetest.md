# 反爬 __极限滑块验证码__ 分析过程

> __某企业信用信息__ 使用了极限验证码，包括 `滑块`和`点选` 验证，这里尝试分析`滑块`验证

[极限文档](https://docs.geetest.com/)

### 搭建极限 [demo](https://docs.geetest.com/sensebot/deploy/server/python) 环境

- 下载 demo

```shell
git clone https://github.com/GeeTeam/gt3-server-python-flask-sdk.git
```

- 运行 demo

```shell
cd gt3-server-python-flask-sdk
sudo pip install -r requirements.txt
sudo python3 app.py
```

> 在浏览器中访问[http://localhost:5000](http://localhost:5000])即可看到demo界面。

### 验证操作

点击验证会出现`滑动块`(也可以不是滑动块验证，不是就重新刷新)

- 滑动滑块，__失败一次__，查看请求:

```
Request URL: http://api.geetest.com/ajax.php?gt=c9c4facd1a6feeb80802222cbb74ca8e&challenge=0963aaa034f481209bcc2cea856f028ckm&lang=zh-cn&pt=0&client_type=web&w=lBwc475p8UVMBWJIGvguOBDGO2XBUVAWcyDMF(cCyHooIvkLJO(kx)4iwEXmKgB(pzX8hmTXVr7s0Cd8zlzLFRKByaxL9Xqebj4jpMbuGxSjMGbCucdrYjEs7hKdhGEHXxIHIrPbJCgS)HCfRtmiI3GeCdF7K7STWgxKzOpzF(osbFpJlFPHPyTq21Haj4LlTwW4PgOYeh(59qOX4aQrDVTLp6M2AonfdeXHlTi652p88Bh9o)blgbRptCo(Btz7JTkPgWBh4bRlBWzWwjxHtogBzRTKiFaQwe4HgvyTwAnxQRPFXt1iWmC7tOr6Ega)MGFZ941TfoV2Ky1yOPOvHAjWbIcCreU3NqYUCTlu3muFqfbdzgynuXQAd04ohUbNS(1MMsLsouhLxs(S4j0wHJWxOT(OBUrVWZ1NYzHNtJ71YoCpwu7LkEaL91cRO9XIYK(ahdEFL9RzWqavlNA(GZTPb8o6yZAEtbCZWFaQoThxcyrlkDluQDOavBCgVJ6LuJxCCjnXINAcNcQk4U6U6mWumJwwZKDGa1UEb2NuIi5Wa)VTMU5gI4YGA)r)FVhWQP9VFvW87MaeGUeDTIylzL1Ic3F7lzU6YqAlL65VzsNETMLw0aLy7x1ofwuZgYmMYrYJYkR1fKLjQULN7BJgH0m6m(TLOLKAt5FQeygWCuaJx54Fzg61XDE8qARsEuhotwezFFdxWUZ1rBQ0yGGfnurouJwsJYooujy(DMSaPeoSLs32ZdxUAdxWHGlnx3uwKLbNwM)KZfEMNyTD))C5ao96cxA3RwKrzH52CE7N9L4.3527385ff911fb8a8f47ab5b6b5b93632dbe721f7d4e80a60e3a4ff7ff8bc037ccffc02d460a5cd6851e6812fe9f15b39fe9dc0cbf2fefc740e2460efe9c4b31be06448b52eb98304bd65f43773f067579743e4f1f978d0bf66347f49f83d965132642922f49e9ef4d192470ef844824d6df28f0c271b9d7d6e47462c13c4a76&callback=geetest_1600681650648
Request Method: GET
Status Code: 200 OK
Remote Address: 203.107.32.16:80
Referrer Policy: no-referrer-when-downgrade

Query String Parameters ->
gt: c9c4facd1a6feeb80802222cbb74ca8e
challenge: f340a03dc45ca5a598ba928f6b103d54k5
lang: zh-cn
pt: 0
client_type: web
w: lBwc475p8UVMBWJIGvguOBDGO2XBUVAWcyDMF(cCyHooIvkLJO(kx)4iwEXmKgB(pzX8hmTXVr7s0Cd8zlzLFRKByaxL9Xqebj4jpMbuGxSjMGbCucdrYjEs7hKdhGEHXxIHIrPbJCgS)HCfRtmiI3GeCdF7K7STWgxKzOpzF(osbFpJlFPHPyTq21Haj4LlTwW4PgOYeh(59qOX4aQrDVTLp6M2AonfdeXHlTi652p88Bh9o)blgbRptCo(Btz7JTkPgWBh4bRlBWzWwjxHtogBzRTKiFaQwe4HgvyTwAnxQRPFXt1iWmC7tOr6Ega)MGFZ941TfoV2Ky1yOPOvHAjWbIcCreU3NqYUCTlu3muFqfbdzgynuXQAd04ohUbNS(1MMsLsouhLxs(S4j0wHJWxOT(OBUrVWZ1NYzHNtJ71YoCpwu7LkEaL91cRO9XIYK(ahdEFL9RzWqavlNA(GZTPb8o6yZAEtbCZWFaQoThxcyrlkDluQDOavBCgVJ6LuJxCCjnXINAcNcQk4U6U6mWumJwwZKDGa1UEb2NuIi5Wa)VTMU5gI4YGA)r)FVhWQP9VFvW87MaeGUeDTIylzL1Ic3F7lzU6YqAlL65VzsNETMLw0aLy7x1ofwuZgYmMYrYJYkR1fKLjQULN7BJgH0m6m(TLOLKAt5FQeygWCuaJx54Fzg61XDE8qARsEuhotwezFFdxWUZ1rBQ0yGGfnurouJwsJYooujy(DMSaPeoSLs32ZdxUAdxWHGlnx3uwKLbNwM)KZfEMNyTD))C5ao96cxA3RwKrzH52CE7N9L4.3527385ff911fb8a8f47ab5b6b5b93632dbe721f7d4e80a60e3a4ff7ff8bc037ccffc02d460a5cd6851e6812fe9f15b39fe9dc0cbf2fefc740e2460efe9c4b31be06448b52eb98304bd65f43773f067579743e4f1f978d0bf66347f49f83d965132642922f49e9ef4d192470ef844824d6df28f0c271b9d7d6e47462c13c4a76
callback: geetest_1600681650648

Response:
Headers ->
Cache-Control: no-cache, no-store, must-revalidate
Connection: keep-alive
Content-Length: 56
Content-Type: text/javascript;charset=UTF-8
Date: Mon, 21 Sep 2020 09:47:24 GMT
Etag: "1dbd74d80dd411079f44b61057458859a2970afd"
Expires: 0
Pragma: no-cache
Server: openresty
Set-Cookie: GeeTestAjaxUser=68748bcc8d5e210678ab924a2d2f1043; expires=Tue, 21 Sep 2021 09:47:24 GMT; Path=/

Body ->
geetest_1600681650648({"success": 0, "message": "fail"})
```

- 滑动滑块到指定位置，__成功验证__，查看请求:

```
Request URL: http://api.geetest.com/ajax.php?gt=c9c4facd1a6feeb80802222cbb74ca8e&challenge=0963aaa034f481209bcc2cea856f028ckm&lang=zh-cn&pt=0&client_type=web&w=lBwc475p8UVMBWJIGvguOBDGO2XBUVAWcyDMF(cCyHpwPSUixa377V2sfcEsuY)g5K0mchqtk(AOV5EcQzckO9(fpM0Nmgc2DFA(lCFNEhCgIqgR7uVwzrEkAhRUyGGui90EXJPerk5aJtf(XTFPQIha9YoXFKWl8KMlhkc5Y3m6G1jFSr19sBF3Z8IGpU2QINew3Nmeb(04bYoYD(s6keOfCSqJvYos6(HbkL)EUDMoZXolzFtLq39fHDZMLdkkvUZy)IenedIr9EnVYk5sdw1BUHlzlbXOjuo1tHiDJcZEeK7RQdodzPFOi8XgTn)8LDNjJftVeKWsjZ7qN)vgvAHZTCjDJkqVwtJJWGJnFKRMlaBfwgf8SNtMWPFts8(DUldMyHMoxEZlHQymWykbweLRFQvUCc3ZbnpG1v14BEp)9EauzMum5I8w(5ENDWGTAtt7fE3CoCuaYK8j2wSKFnzBxssv1kZiJFEbthpk1pGFm8w)O1ZOlehVSePtBYmbGWd)sn1sLUYjB5YISR597W2FdizbmZR0EkXRcIVBZpA(X7moz(FVfGvmWUPDmUCHLjvzC5TY6PMxX6rshBvJx75OdpFlepmBpwM)dJhDvngX4MTiQiilZjEmOQA(gBDOhbSlu4hQuNvlD2R4OkqZp8zyXsCqZTKp0Z9mtd0rP3b(UXeFGfCdaq2gX)Ag0jK9gn)YF8D5KhqUQ6Gfsg8sZNYGAkcJm0tC5rGqewTsD0vM0618aiSZWNouqV7QTBB2G7EmqQt0w7YVc3gu4RmoJmoBU(Kcj8TqkeXLfHww3YeYt(w2vXKKXAt(6xqIbKxpidRTB2InYyhSQOvf5QWQK3r(PL583LQhR3XyOYhUJGSrkDWIzO4mhFMtkxsCRavcA0D365tEM5oHa9vsvY9PTTreDn8olXHqx7hh(e7hRHuNCgVKXzJ0yLIodXP4d5Xed4zuJ6opPB)z4eH5LVxzNQ..ab6515b108b14112eeb500e7c27231f168deded0c0e96572273be4913e1320bf1d485710462c57221e2b95a51a60ce3135a791ea47799450d05c6a0757339931a882b3711363be98f3adb3b0aa05e454b85a542e932553916d10f319db493284c8e038c3a358702df6928de72627d679534af60051172c6024e4c9e72bbebe17&callback=geetest_1600681688549
Request Method: GET
Status Code: 200 OK
Remote Address: 203.107.32.16:80
Referrer Policy: no-referrer-when-downgrade

Query String Parameters ->
gt: c9c4facd1a6feeb80802222cbb74ca8e
challenge: f340a03dc45ca5a598ba928f6b103d54k5
lang: zh-cn
pt: 0
client_type: web
w: lBwc475p8UVMBWJIGvguOBDGO2XBUVAWcyDMF(cCyHpwPSUixa377V2sfcEsuY)g5K0mchqtk(AOV5EcQzckO9(fpM0Nmgc2DFA(lCFNEhCgIqgR7uVwzrEkAhRUyGGui90EXJPerk5aJtf(XTFPQIha9YoXFKWl8KMlhkc5Y3m6G1jFSr19sBF3Z8IGpU2QINew3Nmeb(04bYoYD(s6keOfCSqJvYos6(HbkL)EUDMoZXolzFtLq39fHDZMLdkkvUZy)IenedIr9EnVYk5sdw1BUHlzlbXOjuo1tHiDJcZEeK7RQdodzPFOi8XgTn)8LDNjJftVeKWsjZ7qN)vgvAHZTCjDJkqVwtJJWGJnFKRMlaBfwgf8SNtMWPFts8(DUldMyHMoxEZlHQymWykbweLRFQvUCc3ZbnpG1v14BEp)9EauzMum5I8w(5ENDWGTAtt7fE3CoCuaYK8j2wSKFnzBxssv1kZiJFEbthpk1pGFm8w)O1ZOlehVSePtBYmbGWd)sn1sLUYjB5YISR597W2FdizbmZR0EkXRcIVBZpA(X7moz(FVfGvmWUPDmUCHLjvzC5TY6PMxX6rshBvJx75OdpFlepmBpwM)dJhDvngX4MTiQiilZjEmOQA(gBDOhbSlu4hQuNvlD2R4OkqZp8zyXsCqZTKp0Z9mtd0rP3b(UXeFGfCdaq2gX)Ag0jK9gn)YF8D5KhqUQ6Gfsg8sZNYGAkcJm0tC5rGqewTsD0vM0618aiSZWNouqV7QTBB2G7EmqQt0w7YVc3gu4RmoJmoBU(Kcj8TqkeXLfHww3YeYt(w2vXKKXAt(6xqIbKxpidRTB2InYyhSQOvf5QWQK3r(PL583LQhR3XyOYhUJGSrkDWIzO4mhFMtkxsCRavcA0D365tEM5oHa9vsvY9PTTreDn8olXHqx7hh(e7hRHuNCgVKXzJ0yLIodXP4d5Xed4zuJ6opPB)z4eH5LVxzNQ..ab6515b108b14112eeb500e7c27231f168deded0c0e96572273be4913e1320bf1d485710462c57221e2b95a51a60ce3135a791ea47799450d05c6a0757339931a882b3711363be98f3adb3b0aa05e454b85a542e932553916d10f319db493284c8e038c3a358702df6928de72627d679534af60051172c6024e4c9e72bbebe17
callback: geetest_1600681688549

Response:
Headers ->
Cache-Control: no-cache, no-store, must-revalidate
Connection: keep-alive
Content-Length: 122
Content-Type: text/javascript;charset=UTF-8
Date: Mon, 21 Sep 2020 09:47:59 GMT
Etag: "4a84b96c134bca4790a492cd523901cacbd03e42"
Expires: 0
Pragma: no-cache
Server: openresty
Set-Cookie: GeeTestAjaxUser=1d06456d0b40269eb520a07cba20918d; expires=Tue, 21 Sep 2021 09:47:59 GMT; Path=/

Body ->
geetest_1600681688549({"validate": "44829c7e0b5085bbab69eeb0946880d2", "message": "success", "success": 1, "score": "12"})
```

#### 对比两个响应
 
- 成功响应 返回携带 `validate`

`validate`验证码应该就是我们需要的，请求是通过 `GET` `ajax.php`获取，携带Query参数，看看几个参数：

```
// 失败时
gt: c9c4facd1a6feeb80802222cbb74ca8e
challenge: f340a03dc45ca5a598ba928f6b103d54k5
lang: zh-cn
pt: 0
client_type: web
w: lBwc475p8UVMBWJIGvguOBDGO2XBUVAWcyDMF(cCyHooIvkLJO(kx)4iwEXmKgB(pzX8hmTXVr7s0Cd8zlzLFRKByaxL9Xqebj4jpMbuGxSjMGbCucdrYjEs7hKdhGEHXxIHIrPbJCgS)HCfRtmiI3GeCdF7K7STWgxKzOpzF(osbFpJlFPHPyTq21Haj4LlTwW4PgOYeh(59qOX4aQrDVTLp6M2AonfdeXHlTi652p88Bh9o)blgbRptCo(Btz7JTkPgWBh4bRlBWzWwjxHtogBzRTKiFaQwe4HgvyTwAnxQRPFXt1iWmC7tOr6Ega)MGFZ941TfoV2Ky1yOPOvHAjWbIcCreU3NqYUCTlu3muFqfbdzgynuXQAd04ohUbNS(1MMsLsouhLxs(S4j0wHJWxOT(OBUrVWZ1NYzHNtJ71YoCpwu7LkEaL91cRO9XIYK(ahdEFL9RzWqavlNA(GZTPb8o6yZAEtbCZWFaQoThxcyrlkDluQDOavBCgVJ6LuJxCCjnXINAcNcQk4U6U6mWumJwwZKDGa1UEb2NuIi5Wa)VTMU5gI4YGA)r)FVhWQP9VFvW87MaeGUeDTIylzL1Ic3F7lzU6YqAlL65VzsNETMLw0aLy7x1ofwuZgYmMYrYJYkR1fKLjQULN7BJgH0m6m(TLOLKAt5FQeygWCuaJx54Fzg61XDE8qARsEuhotwezFFdxWUZ1rBQ0yGGfnurouJwsJYooujy(DMSaPeoSLs32ZdxUAdxWHGlnx3uwKLbNwM)KZfEMNyTD))C5ao96cxA3RwKrzH52CE7N9L4.3527385ff911fb8a8f47ab5b6b5b93632dbe721f7d4e80a60e3a4ff7ff8bc037ccffc02d460a5cd6851e6812fe9f15b39fe9dc0cbf2fefc740e2460efe9c4b31be06448b52eb98304bd65f43773f067579743e4f1f978d0bf66347f49f83d965132642922f49e9ef4d192470ef844824d6df28f0c271b9d7d6e47462c13c4a76
callback: geetest_1600681650648

// 成功时
gt: c9c4facd1a6feeb80802222cbb74ca8e
challenge: f340a03dc45ca5a598ba928f6b103d54k5
lang: zh-cn
pt: 0
client_type: web
w: lBwc475p8UVMBWJIGvguOBDGO2XBUVAWcyDMF(cCyHpwPSUixa377V2sfcEsuY)g5K0mchqtk(AOV5EcQzckO9(fpM0Nmgc2DFA(lCFNEhCgIqgR7uVwzrEkAhRUyGGui90EXJPerk5aJtf(XTFPQIha9YoXFKWl8KMlhkc5Y3m6G1jFSr19sBF3Z8IGpU2QINew3Nmeb(04bYoYD(s6keOfCSqJvYos6(HbkL)EUDMoZXolzFtLq39fHDZMLdkkvUZy)IenedIr9EnVYk5sdw1BUHlzlbXOjuo1tHiDJcZEeK7RQdodzPFOi8XgTn)8LDNjJftVeKWsjZ7qN)vgvAHZTCjDJkqVwtJJWGJnFKRMlaBfwgf8SNtMWPFts8(DUldMyHMoxEZlHQymWykbweLRFQvUCc3ZbnpG1v14BEp)9EauzMum5I8w(5ENDWGTAtt7fE3CoCuaYK8j2wSKFnzBxssv1kZiJFEbthpk1pGFm8w)O1ZOlehVSePtBYmbGWd)sn1sLUYjB5YISR597W2FdizbmZR0EkXRcIVBZpA(X7moz(FVfGvmWUPDmUCHLjvzC5TY6PMxX6rshBvJx75OdpFlepmBpwM)dJhDvngX4MTiQiilZjEmOQA(gBDOhbSlu4hQuNvlD2R4OkqZp8zyXsCqZTKp0Z9mtd0rP3b(UXeFGfCdaq2gX)Ag0jK9gn)YF8D5KhqUQ6Gfsg8sZNYGAkcJm0tC5rGqewTsD0vM0618aiSZWNouqV7QTBB2G7EmqQt0w7YVc3gu4RmoJmoBU(Kcj8TqkeXLfHww3YeYt(w2vXKKXAt(6xqIbKxpidRTB2InYyhSQOvf5QWQK3r(PL583LQhR3XyOYhUJGSrkDWIzO4mhFMtkxsCRavcA0D365tEM5oHa9vsvY9PTTreDn8olXHqx7hh(e7hRHuNCgVKXzJ0yLIodXP4d5Xed4zuJ6opPB)z4eH5LVxzNQ..ab6515b108b14112eeb500e7c27231f168deded0c0e96572273be4913e1320bf1d485710462c57221e2b95a51a60ce3135a791ea47799450d05c6a0757339931a882b3711363be98f3adb3b0aa05e454b85a542e932553916d10f319db493284c8e038c3a358702df6928de72627d679534af60051172c6024e4c9e72bbebe17
callback: geetest_1600681688549
```

参数 |  ok | fail
--- | --- | ---
gt  | c9c4facd1a6feeb80802222cbb74ca8e | c9c4facd1a6feeb80802222cbb74ca8e
challenge | f340a03dc45ca5a598ba928f6b103d54k5 | f340a03dc45ca5a598ba928f6b103d54k5
lang | zh-cn | zh-cn
pt | 0 | 0
client_type | web | web
w | lBwc475p8UVMBWJIGvguOBDGO2XBUVAWcyDMF(cCyHooIvkLJO(kx)4iwEXmKgB(pzX8hmTXVr7s0Cd8zlzLFRKByaxL9Xqebj4jpMbuGxSjMGbCucdrYjEs7hKdhGEHXxIHIrPbJCgS)HCfRtmiI3GeCdF7K7STWgxKzOpzF(osbFpJlFPHPyTq21Haj4LlTwW4PgOYeh(59qOX4aQrDVTLp6M2AonfdeXHlTi652p88Bh9o)blgbRptCo(Btz7JTkPgWBh4bRlBWzWwjxHtogBzRTKiFaQwe4HgvyTwAnxQRPFXt1iWmC7tOr6Ega)MGFZ941TfoV2Ky1yOPOvHAjWbIcCreU3NqYUCTlu3muFqfbdzgynuXQAd04ohUbNS(1MMsLsouhLxs(S4j0wHJWxOT(OBUrVWZ1NYzHNtJ71YoCpwu7LkEaL91cRO9XIYK(ahdEFL9RzWqavlNA(GZTPb8o6yZAEtbCZWFaQoThxcyrlkDluQDOavBCgVJ6LuJxCCjnXINAcNcQk4U6U6mWumJwwZKDGa1UEb2NuIi5Wa)VTMU5gI4YGA)r)FVhWQP9VFvW87MaeGUeDTIylzL1Ic3F7lzU6YqAlL65VzsNETMLw0aLy7x1ofwuZgYmMYrYJYkR1fKLjQULN7BJgH0m6m(TLOLKAt5FQeygWCuaJx54Fzg61XDE8qARsEuhotwezFFdxWUZ1rBQ0yGGfnurouJwsJYooujy(DMSaPeoSLs32ZdxUAdxWHGlnx3uwKLbNwM)KZfEMNyTD))C5ao96cxA3RwKrzH52CE7N9L4.3527385ff911fb8a8f47ab5b6b5b93632dbe721f7d4e80a60e3a4ff7ff8bc037ccffc02d460a5cd6851e6812fe9f15b39fe9dc0cbf2fefc740e2460efe9c4b31be06448b52eb98304bd65f43773f067579743e4f1f978d0bf66347f49f83d965132642922f49e9ef4d192470ef844824d6df28f0c271b9d7d6e47462c13c4a76 | lBwc475p8UVMBWJIGvguOBDGO2XBUVAWcyDMF(cCyHpwPSUixa377V2sfcEsuY)g5K0mchqtk(AOV5EcQzckO9(fpM0Nmgc2DFA(lCFNEhCgIqgR7uVwzrEkAhRUyGGui90EXJPerk5aJtf(XTFPQIha9YoXFKWl8KMlhkc5Y3m6G1jFSr19sBF3Z8IGpU2QINew3Nmeb(04bYoYD(s6keOfCSqJvYos6(HbkL)EUDMoZXolzFtLq39fHDZMLdkkvUZy)IenedIr9EnVYk5sdw1BUHlzlbXOjuo1tHiDJcZEeK7RQdodzPFOi8XgTn)8LDNjJftVeKWsjZ7qN)vgvAHZTCjDJkqVwtJJWGJnFKRMlaBfwgf8SNtMWPFts8(DUldMyHMoxEZlHQymWykbweLRFQvUCc3ZbnpG1v14BEp)9EauzMum5I8w(5ENDWGTAtt7fE3CoCuaYK8j2wSKFnzBxssv1kZiJFEbthpk1pGFm8w)O1ZOlehVSePtBYmbGWd)sn1sLUYjB5YISR597W2FdizbmZR0EkXRcIVBZpA(X7moz(FVfGvmWUPDmUCHLjvzC5TY6PMxX6rshBvJx75OdpFlepmBpwM)dJhDvngX4MTiQiilZjEmOQA(gBDOhbSlu4hQuNvlD2R4OkqZp8zyXsCqZTKp0Z9mtd0rP3b(UXeFGfCdaq2gX)Ag0jK9gn)YF8D5KhqUQ6Gfsg8sZNYGAkcJm0tC5rGqewTsD0vM0618aiSZWNouqV7QTBB2G7EmqQt0w7YVc3gu4RmoJmoBU(Kcj8TqkeXLfHww3YeYt(w2vXKKXAt(6xqIbKxpidRTB2InYyhSQOvf5QWQK3r(PL583LQhR3XyOYhUJGSrkDWIzO4mhFMtkxsCRavcA0D365tEM5oHa9vsvY9PTTreDn8olXHqx7hh(e7hRHuNCgVKXzJ0yLIodXP4d5Xed4zuJ6opPB)z4eH5LVxzNQ..ab6515b108b14112eeb500e7c27231f168deded0c0e96572273be4913e1320bf1d485710462c57221e2b95a51a60ce3135a791ea47799450d05c6a0757339931a882b3711363be98f3adb3b0aa05e454b85a542e932553916d10f319db493284c8e038c3a358702df6928de72627d679534af60051172c6024e4c9e72bbebe17
callback | geetest_1600681650648 | geetest_1600681688549

- `gt`, `challenge` 是一样的，主要需要分析它们是来自哪里
- `lang`, `pt`, `client_type` 是固定值
- `callback` 是**geetest_[时间戳]**，返回响应就是对应的**geetest_[时间戳]**
- `w` 值很复杂，应该就是 滑块的`运动轨迹`相关生成的

### 分析参数来历

刷新网页，查看`Chrome`的`Network`

#### 请求 GET register:

```
Request URL: http://0.0.0.0:5000/register?t=1600703538795
Request Method: GET

Response:
{
    "success":1,
    "gt":"c9c4facd1a6feeb80802222cbb74ca8e",
    "challenge":"f340a03dc45ca5a598ba928f6b103d54",
    "new_captcha":true
}
```

- Query参数 `t=1600703538795`

    ![t=1600703538795](images/C6A8D100-E2FC-453B-9F62-2793FAF23AFE.png)

可以看出`register 请求`里面有`gt`, `challenge`

gt 是不变的，而且每次请求都是一样的

查看 `gt3-server-python-flask-sdk` 的 `geetest_config.py`:

```py
GEETEST_ID = "c9c4facd1a6feeb80802222cbb74ca8e"
GEETEST_KEY = "e4e298788aa8c768397639deb9b249a9"
```

__发现`gt`就是 `GEETEST_ID`__


但`challenge` 值和 `ajax.php` 请求的值是不一样的

追踪`ajax.php`请求回调:

![](images/853E08D9-B26B-4039-B4E1-7B4A41B0FDDB.png)
![](images/7BA8F28B-082C-4FE9-B5D8-86DAC2475616.png)
![](images/3733C757-B620-42DE-A544-1B279C6FD3EE.png)
![](images/AA32CD3A-5282-4040-A2AD-68CA3AD45CFE.png)
![](images/AD8B903E-120B-4DB0-807F-0DD5E09C47BF.png)
![](images/E9A00451-B2AD-41E3-9698-C112789B8139.png)
![](images/38C753B1-D3D2-4A01-8749-32C761EB2E9C.png)

```js
u = {
    "\u0067\u0074": o[$_CAICh(171)],
    "\u0063\u0068\u0061\u006c\u006c\u0065\u006e\u0067\u0065": o[$_CAIBT(128)],
    "\u006c\u0061\u006e\u0067": i[$_CAICh(111)],
    "\u0070\u0074": r[$_CAICh(630)],
    "\u0063\u006c\u0069\u0065\u006e\u0074\u005f\u0074\u0079\u0070\u0065": r[$_CAICh(663)],
    "\u0077": _ + s
};
```

Unicode | 字符串 
-- | --
\u0067\u0074 | gt
\u0063\u0068\u0061\u006c\u006c\u0065\u006e\u0067\u0065 | challenge
\u006c\u0061\u006e\u0067 | lang
\u0070\u0074 | pt
\u0063\u006c\u0069\u0065\u006e\u0074\u005f\u0074\u0079\u0070\u0065 | client_type
\u0077 | w


- 查看`gt`

```sh
> $_CAICh(171)
"gt"
> o[$_CAICh(171)]
"c9c4facd1a6feeb80802222cbb74ca8e"
```

`gt` 就是取`o`对象中的 __gt__ 属性，等效 `u.gt = o.gt`

- 查看`challenge`:

```sh
> $_CAIBT(128)
"challenge"
> o[$_CAIBT(128)]
"f5ec9b7bf21fa0e8bcc78bad646a60298e"
```

在此`challenge`已经生成好了，但它是怎么生成的呢？

```js
o = r[$_CAIBT(24)]

> $_CAIBT(24)
"$_IGz"

o = r.$_IGz

> r.$_IGz
$_GIK
```

搜索`$_GIK`相关的代码

`challenge` 的Unicode值是 
`\u0063\u0068\u0061\u006c\u006c\u0065\u006e\u0067\u0065`

```js
AGWEz.$_Cc(128) == `challenge` 
```

源码有`challenge` 相关的属性都是其它对象的`challenge`的属性获得
猜测需要的`challenge`可能是从服务器返回对象中获得

查看其它请求，发现`get.php`:

```js
Request URL: http://api.geetest.com/get.php?is_next=true&type=slide3&gt=c9c4facd1a6feeb80802222cbb74ca8e&challenge=f7efd292522ee8db244f04e74d95e7f4&lang=zh-cn&https=false&protocol=http%3A%2F%2F&offline=false&product=embed&api_server=api.geetest.com&isPC=true&width=100%25&callback=geetest_1600829591801
Request Method: GET
Status Code: 200 OK

Query Parameters:
is_next: true
type: slide3
gt: c9c4facd1a6feeb80802222cbb74ca8e
challenge: f7efd292522ee8db244f04e74d95e7f4
lang: zh-cn
https: false
protocol: http://
offline: false
product: embed
api_server: api.geetest.com
isPC: true
width: 100%
callback: geetest_1600829591801

Response:

Headers ->
Cache-Control: no-cache, no-store, must-revalidate
Connection: keep-alive
Content-Length: 1546
Content-Type: text/javascript;charset=UTF-8
Date: Wed, 23 Sep 2020 02:53:07 GMT
Etag: "974d863efd7b406d9d1d80807c5d9db5960ed5ef"
Expires: 0
Pragma: no-cache
Server: openresty
Set-Cookie: GeeTestUser=7dd27d058b44300de889a2f9ac14a0d1; expires=Thu, 23 Sep 2021 02:53:07 GMT; Path=/

Body ->
geetest_1600829591801({
    "benchmark": false,
    "c": [12, 58, 98, 36, 43, 95, 62, 15, 12],
    "id": "af7efd292522ee8db244f04e74d95e7f4",
    "version": "6.0.9",
    "show_delay": 250,
    "mobile": true,
    "challenge": "f7efd292522ee8db244f04e74d95e7f4fn",
    "bg": "pictures/gt/d49a453dc/bg/655b485ef.jpg",
    "clean": false,
    "static_servers": ["static.geetest.com/", "dn-staticdown.qbox.me/"],
    "type": "multilink",
    "fullbg": "pictures/gt/d49a453dc/d49a453dc.jpg",
    "theme": "ant",
    "gt": "c9c4facd1a6feeb80802222cbb74ca8e",
    "so": 0,
    "xpos": 0,
    "s": "62775164",
    "theme_version": "1.2.4",
    "api_server": "http://api.geetest.com/",
    "template": "",
    "hide_delay": 800,
    "link": "",
    "ypos": 65,
    "i18n_labels": {
        "fail": "\u8bf7\u6b63\u786e\u62fc\u5408\u56fe\u50cf",
        "voice": "\u89c6\u89c9\u969c\u788d",
        "loading": "\u52a0\u8f7d\u4e2d...",
        "feedback": "\u5e2e\u52a9\u53cd\u9988",
        "close": "\u5173\u95ed\u9a8c\u8bc1",
        "error": "\u8bf7\u91cd\u8bd5",
        "logo": "\u7531\u6781\u9a8c\u63d0\u4f9b\u6280\u672f\u652f\u6301",
        "slide": "\u62d6\u52a8\u6ed1\u5757\u5b8c\u6210\u62fc\u56fe",
        "cancel": "\u53d6\u6d88",
        "success": "sec \u79d2\u7684\u901f\u5ea6\u8d85\u8fc7 score% \u7684\u7528\u6237",
        "refresh": "\u5237\u65b0\u9a8c\u8bc1",
        "read_reversed": false,
        "forbidden": "\u602a\u7269\u5403\u4e86\u62fc\u56fe\uff0c\u8bf7\u91cd\u8bd5",
        "tip": "\u8bf7\u5b8c\u6210\u4e0b\u65b9\u9a8c\u8bc1"
    },
    "slice": "pictures/gt/d49a453dc/slice/655b485ef.png",
    "logo": true,
    "width": "100%",
    "feedback": "http://www.geetest.com/contact#report",
    "product": "embed",
    "height": 160,
    "https": false,
    "fullpage": false
})
```

响应的`challenge`值即为需要的

> `get.php`请求发送了两次，两次的请求参数不一样，第一次的响应没有携带`challenge`值
> 第一次是刷新

- 查看`lang`:

```sh
> $_CAICh(111)
"lang"
> i[$_CAICh(111)]
"zh-cn"
```

- 查看`pt`:

```sh
> $_CAICh(630)
"$_CDGy"
> r[$_CAICh(630)]
0
```

- 查看`client_type`:

```sh
> $_CAICh(663)
"$_CDHI"
> r[$_CAICh(663)]
"web"
```

- 查看`w`:

```js
var s = r[$_CAIBT(795)]()
    , a = AES[$_CAIBT(339)](gjson[$_CAICh(242)](i), r[$_CAICh(774)]())
    , _ = Base64[$_CAICh(725)](a)
w = _ + s
```

查看对应的值:

```js
> $_CAIBT(795)
"$_CIAc"
> $_CAIBT(339)
"encrypt"
> $_CAICh(725)
"$_BBAI"
> $_CAICh(242)
"stringify"
> $_CAICh(774)
"$_CIBb"

> i
{
    "lang":"zh-cn",
    "userresponse":"aa4a49166",
    "passtime":610,
    "imgload":15915,
    "aa":"Lw(!!Ktsyttstststsossts:sss(!!($*H2191111203/4/3827?-$-6",
    "ep":{
        "v":"7.7.4",
        "te":false,
        "me":true,
        "tm":{
            "a":1600754998393,
            "b":1600754998444,
            "c":1600754998444,
            "d":0,
            "e":0,
            "f":1600754998398,
            "g":1600754998398,
            "h":1600754998398,
            "i":1600754998398,
            "j":1600754998398,
            "k":0,
            "l":1600754998419,
            "m":1600754998436,
            "n":1600754998438,
            "o":1600754998493,
            "p":1600755004551,
            "q":1600755004551,
            "r":1600755004553,
            "s":1600755004574,
            "t":1600755004574,
            "u":1600755004574
        },
        "td":-1
    },
    "rp":"af19f9fedba491ac4c017780ca07db72"
}
```

简单的反混淆后的代码：

```js
var s = r.$_CIAc(),
    a = AES.encrypt(gjson.stringify(i), r.$_CIBb()),
    _ = Base64.$_BBAI(a)
w = _ + s 
```

获取`s`值的函数：

```js
> r.$_CIAc
ƒ (t){var $_CBEGF=AGWEz.$_Cc,$_CBEFR=['$_CBEJN'].concat($_CBEGF),$_CBEHp=$_CBEFR[1];$_CBEFR.shift();var $_CBEIS=$_CBEFR[0];var e=new RSAKey()[$_CBEHp(339)](this[$_CBEHp(774)](t));while(!e||256!==e[$_CB…
```

点击函数可以跳转的函数对应的位置：

```js
"\u0024\u005f\u0043\u0049\u0041\u0063": function(t) {
    var $_CBEGF = AGWEz.$_Cc
        , $_CBEFR = ['$_CBEJN'].concat($_CBEGF)
        , $_CBEHp = $_CBEFR[1];
    $_CBEFR.shift();
    var $_CBEIS = $_CBEFR[0];
    var e = new RSAKey()[$_CBEHp(339)](this[$_CBEHp(774)](t));
    while (!e || 256 !== e[$_CBEGF(124)])
        e = new RSAKey()[$_CBEHp(339)](this[$_CBEGF(774)](!0));
    return e;
}
```

翻译一下:

```js
$_CIAc: function(t) {
    var e = new RSAKey().encrypt(this.$_CIBb(t));
    while (!e || 256 !== e.length)
        e = new RSAKey().encrypt(this.$_CIBb(true));
    return e;
}
```

当`var s = r.$_CIAc()`, _t == undefined_ 可以简化：

```js
$_CIAc: function() {
    return new RSAKey().encrypt(this.$_CIBb(true));;
}
```

`this.$_CIBb` 函数:

```js
function(t) {
    var $_CBEBD = AGWEz.$_Cc
        , $_CBEA_ = ['$_CBEEQ'].concat($_CBEBD)
        , $_CBECX = $_CBEA_[1];
    $_CBEA_.shift();
    var $_CBEDZ = $_CBEA_[0];
    return !0 === t && (Ez = $_DJn()), Ez;
}

// 翻译
function(t) {
    return true === t && (Ez = $_DJn()), Ez;
}

function $_DJn() {
    return Gr() + Gr() + Gr() + Gr();
}

function Gr() {
    return (65536 * (1 + Math.random()) | 0).toString(16).substring(1);
}
```

获取`a`值的函数：

```js
a = AES.encrypt(gjson.stringify(i), r.$_CIBb())

// 翻译
a = AES.encrypt(gjson.stringify(i), randomKey())
```

以中`i`的值:

```js
// r = $_GGL的实例
i = {
    "\u006c\u0061\u006e\u0067": o[$_CAIBT(111)] || $_CAIBT(198),
    "\u0075\u0073\u0065\u0072\u0072\u0065\u0073\u0070\u006f\u006e\u0073\u0065": $_DDc(t, o[$_CAIBT(128)]),
    "\u0070\u0061\u0073\u0073\u0074\u0069\u006d\u0065": n,
    "\u0069\u006d\u0067\u006c\u006f\u0061\u0064": r[$_CAICh(790)],
    "\u0061\u0061": e,
    "\u0065\u0070": r[$_CAIBT(757)]()
};
i[$_CAICh(729)] = $_DCF(o[$_CAIBT(171)] + o[$_CAICh(128)][$_CAICh(62)](0, 32) + i[$_CAIBT(722)]);

// 翻译
i = {
    lang: "zh-cn",
    userresponse: $_DDc(t, o.challenge),
    passtime: n,
    imgload: r.$_CGDi,
    aa: e,
    ep: r.$_CHJF()
}
i.rp = $_DCF(o.gt) + o.challenge.slice(0, 32) + i.passtime
```

`o.challenge 是第二次请求get.php获取的challenge值`
`e`, `n` 来自函数参数， 通过 Call Stack 向上追踪 

```js
var a = n[$_CJHHV(844)]
    , _ = e ? n[$_CJHHV(999)][$_CJHHV(351)] : t[$_CJHHV(936)]() / a - n[$_CJHGj(935)]
    , u = e ? n[$_CJHHV(999)][$_CJHGj(385)] : n[$_CJHGj(995)] - t[$_CJHHV(945)]() / a;
n[$_CJHHV(808)] = $_EGb() - n[$_CJHHV(937)],
                    n[$_CJHHV(981)][$_CJHGj(994)]([Math[$_CJHHV(293)](_), Math[$_CJHHV(293)](u), n[$_CJHGj(808)]]);

l = n[$_CJHHV(981)][$_CJHHV(980)](n[$_CJHHV(981)][$_CJHHV(903)](), n[$_CJHHV(24)][$_CJHHV(1071)], n[$_CJHGj(24)][$_CJHGj(343)]);  // e
n[$_CJHHV(808)]                // n

// 翻译
var a = n.$_DBBS  // 1
    , _ = e ? n.lastPoint.x : t.$_CCAe() / a - n.$_DDHK
    , u = e ? n.lastPoint.y : n.$_DDIF - t.$_CCBs() / a;
n._DAAt = new Date().getTime - n.$_DDGR, 
        n.$_DDJm.$_BHFA([Math.round(_), Math.round(u), n.$_DAAt);

l = n.$_DDJm.$_BHGu(n.$_DDJm.$_BAJk(), n.$_IGz.c, n.$_IGz.s) // e
n.$_DAAt                        // n

// 输出
> n.$_IGz.c
(9) [12, 58, 98, 36, 43, 95, 62, 15, 12]
> n.$_IGz.s
"52657746"
> n.$_DDHK
567.26953125
> n.$_DDGR
1600852141886


"\u0024\u005f\u0042\u0048\u0047\u0075": function(t, e, n) {
    var $_BEJGY = AGWEz.$_Cc
        , $_BEJF_ = ['$_BEJJh'].concat($_BEJGY)
        , $_BEJHQ = $_BEJF_[1];
    $_BEJF_.shift();
    var $_BEJIP = $_BEJF_[0];
    if (!e || !n)
        return t;
    var r, o = 0, i = t, s = e[0], a = e[2], _ = e[4];
    while (r = n[$_BEJHQ(283)](o, 2)) {
        o += 2;
        var u = parseInt(r, 16)
            , c = String[$_BEJHQ(273)](u)
            , l = (s * u * u + a * u + _) % t[$_BEJHQ(124)];
        i = i[$_BEJGY(283)](0, l) + c + i[$_BEJGY(283)](l);
    }
    return i;
}

// 翻译：
// "T(!!Gtttsyttsss(!!($)R5-6,30111$.b", [12, 58, 98, 36, 43, 95, 62, 15, 12], "52657746"
$_BHGu: function(aa, e, n) {
    if (!e || !n)
        return aa;
    var r, index = 0, str = aa, s = e[0], a = e[2], _ = e[4];
    while (r = n.substr(index, 2)) {
        index += 2;
        var u = parseInt(r, 16)
            , c = String.fromCharCode(u)
            , l = (s * u * u + a * u + _) % aa.length;
        str = str.substr(0, l) + c + str.substr(l);
    }
    return str;
}

// "\u0024\u005f\u0042\u0041\u004a\u006b": function() {
$_BAJk: function() {
    function n(t) {
        var e = "()*,-./0123456789:?@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqr",
            n = e.length,
            r = "",
            o = Math.abs(t),
            i = parseInt(o / n);
        n <= i && (i = n - 1), i && (r = e.charAt(i));
        var s = "";
        return t < 0 && (s += "!"), r && (s += "$", s + r + e.charAt(o %= n);
    }
    var t = function(t) {
            for (var e, n, r, o = [], i = 0, s = 0, a = t.length - 1; s < a; s++) {
                e = Math.round(t[s + 1][0] - t[s][0]), 
                n = Math.round(t[s + 1][1] - t[s][1]), 
                r = Math.round(t[s + 1][2] - t[s][2]), 
                0 == e && 0 == n && 0 == r || (0 == e && 0 == n ? i += r : (o.push([e, n, r + i]), i = 0));
            }
            return 0 !== i && o.push([e, n, i]), o;
        }(this.$_BDA_),
        r = [],
        o = [],
        i = [];
    
    return new $_FAU(t).$_HFd(function(t) {
        var e = function(t) {
                for (var e = [
                    [1, 0],
                    [2, 0],
                    [1, -1],
                    [1, 1],
                    [0, 1],
                    [0, -1],
                    [3, 0],
                    [2, -1],
                    [2, 1]
                ], n = 0, r = e.length; n < r; n++) 
                if (t[0] == e[n][0] && t[1] == e[n][1]) return "stuvwxyz~"[n]; 
                return 0;
            }(t);
        e ? o.push(e) : (r.push(n(t[0])), o.push(n(t[1]))), i.push(n(t[2]));
    }), r.join("") + "!!" + o.join("") + "!!" + i.join("");
},

function $_FAU(e) {
    this.$_BHIa = e || [];
}

// $_FAU.prototype.$_HFd
// f 回调函数
//"\u0024\u005f\u0048\u0046\u0064": function(t) {
$_HFd: function(f) {
    var e = this.$_BHIa; // 是 Array(26)
    if (e.map)
        return new $_FAU(e.map(f));
    for (var n = [], r = 0, o = e.length; r < o; r += 1)
        n[r] = f(e[r], r, this);
    return new $_FAU(n);
},

// 整理：
$_BHGu: function(aa, e, n) {
    if (!e || !n)
        return aa;
    var r, index = 0, str = aa, s = e[0], a = e[2], _ = e[4];
    while (r = n.substr(index, 2)) {
        index += 2;
        var u = parseInt(r, 16)
            , c = String.fromCharCode(u)
            , l = (s * u * u + a * u + _) % aa.length;
        str = str.substr(0, l) + c + str.substr(l);
    }
    return str;
}

// "\u0024\u005f\u0042\u0041\u004a\u006b": function() {
$_BAJk: function() {
    function n(t) {
        var e = "()*,-./0123456789:?@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqr",
            n = e.length,
            r = "",
            o = Math.abs(t),
            i = parseInt(o / n);
        n <= i && (i = n - 1), i && (r = e.charAt(i));
        var s = "";
        return t < 0 && (s += "!"), r && (s += "$", s + r + e.charAt(o %= n);
    }
    var t = function(t) {
            for (var e, n, r, o = [], i = 0, s = 0, a = t.length - 1; s < a; s++) {
                e = Math.round(t[s + 1][0] - t[s][0]), 
                n = Math.round(t[s + 1][1] - t[s][1]), 
                r = Math.round(t[s + 1][2] - t[s][2]), 
                0 == e && 0 == n && 0 == r || (0 == e && 0 == n ? i += r : (o.push([e, n, r + i]), i = 0));
            }
            return 0 !== i && o.push([e, n, i]), o;
        }(this.$_BDA_),
        r = [],
        o = [],
        i = [];
    
    return t.map(function(t) {
        var e = function(t) {
                for (var e = [
                    [1, 0],
                    [2, 0],
                    [1, -1],
                    [1, 1],
                    [0, 1],
                    [0, -1],
                    [3, 0],
                    [2, -1],
                    [2, 1]
                ], n = 0, r = e.length; n < r; n++) 
                if (t[0] == e[n][0] && t[1] == e[n][1]) return "stuvwxyz~"[n]; 
                return 0;
            }(t);
        e ? o.push(e) : (r.push(n(t[0])), o.push(n(t[1]))), i.push(n(t[2]));
    }), r.join("") + "!!" + o.join("") + "!!" + i.join("");
},
```

```js
> this.$_BDA_
0: (3) [-36, -27, 0]
1: (3) [0, 0, 0]
2: (3) [1, 0, 226]
3: (3) [3, 0, 233]
4: (3) [6, 0, 241]
5: (3) [6, 0, 249]
6: (3) [7, 0, 257]
7: (3) [9, 0, 265]
8: (3) [10, 0, 274]
9: (3) [11, 0, 283]
10: (3) [12, 0, 289]
11: (3) [13, 0, 299]
12: (3) [14, 0, 305]
13: (3) [15, 0, 316]
14: (3) [16, 0, 321]
15: (3) [17, 0, 333]
16: (3) [18, 0, 338]
17: (3) [20, 0, 350]
18: (3) [21, 0, 354]
19: (3) [22, 0, 367]
20: (3) [23, 0, 370]
21: (3) [24, 0, 383]
22: (3) [25, 0, 386]
23: (3) [25, 0, 394]
24: (3) [26, 0, 402]
25: (3) [26, 0, 412]
26: (3) [27, 0, 418]
27: (3) [27, 0, 427]
28: (3) [28, 0, 434]
29: (3) [28, 0, 443]
30: (3) [28, 0, 450]
31: (3) [28, 0, 475]
32: (3) [29, 0, 533]
33: (3) [29, 0, 588]
length: 34

> t = t(this.$_BDA_)
> t
0: (3) [36, 27, 0]
1: (3) [1, 0, 226]
2: (3) [2, 0, 7]
3: (3) [3, 0, 8]
4: (3) [1, 0, 16]
5: (3) [2, 0, 8]
6: (3) [1, 0, 9]
7: (3) [1, 0, 9]
8: (3) [1, 0, 6]
9: (3) [1, 0, 10]
10: (3) [1, 0, 6]
11: (3) [1, 0, 11]
12: (3) [1, 0, 5]
13: (3) [1, 0, 12]
14: (3) [1, 0, 5]
15: (3) [2, 0, 12]
16: (3) [1, 0, 4]
17: (3) [1, 0, 13]
18: (3) [1, 0, 3]
19: (3) [1, 0, 13]
20: (3) [1, 0, 3]
21: (3) [1, 0, 16]
22: (3) [1, 0, 16]
23: (3) [1, 0, 16]
24: (3) [1, 0, 99]
25: (3) [0, 0, 55]
length: 26
```

`n.$_IGz.c`:

第二次 `get.php`请求的 `c`属性

`n.$_IGz.s`:

第二次 `get.php`请求的 `s`属性

`n._DAAt`:

```js
var a = n.$_DBBS  // 1
    , _ = e ? n.lastPoint.x : t.$_CCAe() / a - n.$_DDHK
    , u = e ? n.lastPoint.y : n.$_DDIF - t.$_CCBs() / a;
n._DAAt = new Date().getTime - n.$_DDGR, 
        n.$_DDJm.$_BHFA([Math.round(_), Math.round(u), n.$_DAAt);
```

```js
// "\u0024\u005f\u0043\u0043\u0041\u0065": function() {
$_CCAe: function() {
    var t = this.$_BBIM; // PointerEvent {isTrusted: true, pointerId: 1, width: 1, height: 1, pressure: 0.5, …}
    if ("number" == typeof (t.clientX))
        return t.clientX;
    var e = t.changedTouches && t.changedTouches[0];
    return e ? e.clientX : -1;
},

// "\u0024\u005f\u0043\u0043\u0042\u0073": function() {
$_CCBs: : function() {
    var t = this.$_BBIM; // PointerEvent {isTrusted: true, pointerId: 1, width: 1, height: 1, pressure: 0, …}
    if ("number" == typeof (t.clientY))
        return t.clientY;
    var e = t.changedTouches && t.changedTouches[0];
    return e ? e.clientY : -1;
},

// "\u0024\u005f\u0042\u0048\u0046\u0041": function(t) {
// t -> [22, -2, 8911]
$_BHFA: function(t) {
    return this.$_BDA_.push(t), this;   // 
},
```

猜测 鼠标落下时事件的值:
`n.$_DDHK` 就是获取 PointerEvent的clientX
`n.$_DDIF` 就是获取 PointerEvent的clientY
`n.$_DDGR` 是`new Date().getTime`


整理：

```js
i = {
    lang: "zh-cn",
    userresponse: $_DDc(t, o.challenge),
    passtime: n,
    imgload: r.$_CGDi,
    aa: e,
    ep: r.$_CHJF()
}
i.rp = $_DCF(o.gt) + o.challenge.slice(0, 32) + i.passtime

var s = new RSAKey().encrypt(randomKey()),
    a = AES.encrypt(gjson.stringify(i), randomKey()),
    _ = Base64.$_BBAI(a)
w = _ + s 

function randomKey() {
    return Gr() + Gr() + Gr() + Gr();
}

function Gr() {
    return (65536 * (1 + Math.random()) | 0).toString(16).substring(1);
}

```


#### 滑块


--- 

[js逆向技巧分享](https://zhuanlan.zhihu.com/p/108207751)
[极验验证码破解之selenium](https://www.jianshu.com/p/c8df1194b514)
[极验证码识别http接口](https://www.kancloud.cn/abcabc123/yiyunapi/1879968)(打码平台)
[极验证码文档](https://docs.geetest.com/sensebot/deploy/server/python)
[极验验证码破解](https://zhuanlan.zhihu.com/p/28493013)
[反爬经验](https://www.zhihu.com/people/boris-97-17/posts)
[学web安全去哪里找各种各样的靶场？](https://www.zhihu.com/question/267204109/answer/320502511)
[CTF-Platforms](https://github.com/We5ter/Awesome-Platforms/blob/master/CTF-Platforms.md)